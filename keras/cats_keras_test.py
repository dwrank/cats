#!/usr/bin/env python

from keras.models import load_model
from utils import load_data, predict_accuracy


if __name__ == '__main__':
    # Load the data
    _, _, test_x_orig, test_y, _ = load_data()

    # Reshape the training and test examples 
    test_x_flatten = test_x_orig.reshape(test_x_orig.shape[0], -1).T

    # Standardize data to have feature values between 0 and 1.
    test_x = test_x_flatten/255.

    m = test_x_orig.shape[0]  # training set size

    print ('The shape of X is: ' + str(test_x_orig.shape))
    print ('I have m = %d training examples!' % (m))

    # load the model
    model = load_model('cats_keras.h5')

    # evaluate the model
    X = test_x
    Y = test_y
    scores = model.evaluate(X.T, Y.T)
    print("\n%s: %.2f%%" % (model.metrics_names[1], scores[1]*100))

    predict_accuracy('Keras NN', model, X, Y)    
    
    del model

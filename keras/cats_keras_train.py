#!/usr/bin/env python

import numpy as np
from keras.models import Sequential
from keras.layers import Dense
from datetime import datetime

from utils import load_data, predict_accuracy


np.random.seed(1) # set a seed so that the results are consistent


# create NN model
def create_nn_model(layers):
    model = Sequential()

    for i in range(len(layers)-2):
        model.add(Dense(layers[i+1], input_dim=layers[i], activation='relu'))

    model.add(Dense(layers[-1], activation='sigmoid'))
    
    # Compile model
    model.compile(loss='mse', optimizer='rmsprop', metrics=['accuracy'])
    
    return model


def train_model(train_x, train_y, test_x, test_y, model, epochs, batch_size):
    print('\n' + '-' * 80)
    print('Train Model, epochs: %u, batch size: %u')

    t1 = datetime.now()
    X = train_x
    Y = train_y
    model.fit(X.T, Y.T, epochs=epochs, batch_size=batch_size)

    # evaluate the model
    if 1:
        scores = model.evaluate(X.T, Y.T)
        print("\n%s: %.2f%%" % (model.metrics_names[1], scores[1]*100))

        train_acc = predict_accuracy('Keras NN Train', model, X, Y)

        X = test_x
        Y = test_y
        scores = model.evaluate(X.T, Y.T)
        print("\n%s: %.2f%%" % (model.metrics_names[1], scores[1]*100))

        test_acc = predict_accuracy('Keras NN Test', model, X, Y)

    t2 = datetime.now()
    d = {}
    d['Epochs'] = epochs
    d['Batch Size'] = batch_size
    d['Train Acc'] = train_acc
    d['Test Acc'] = test_acc
    d['Time'] = t2-t1
    return d


if __name__ == '__main__':
    # Load the data
    train_x_orig, train_y, test_x_orig, test_y, _ = load_data()

    # Reshape the training and test examples 
    train_x_flatten = train_x_orig.reshape(train_x_orig.shape[0], -1).T   # The "-1" makes reshape flatten the remaining dimensions
    test_x_flatten = test_x_orig.reshape(test_x_orig.shape[0], -1).T

    # Standardize data to have feature values between 0 and 1.
    train_x = train_x_flatten/255.
    test_x = test_x_flatten/255.

    m = train_x_orig.shape[0]  # training set size

    print ('The shape of X is: ' + str(train_x_orig.shape))
    print ('I have m = %d training examples!' % (m))

    layers = [12288, 20, 7, 5, 1]
    #epochs = 100#2500
    #batch_size = 10 #m
    params = [[100,20]]#[[100,10], [100,20], [100,30], [50,50], [50,25], [50,10]]
    model = create_nn_model(layers)

    # Fit the model
    results = []
    for p in params:
        epochs = p[0]
        batch_size = p[1]
        d = train_model(train_x, train_y, test_x, test_y, model, epochs, batch_size)
        results.append(d)

    for d in results:
        print('-' * 80)
        for key in d:
            print('%s: %s ' % (key, str(d[key])))
        
    # Save the model
    model.save('cats_keras.h5')
    del model

#!/usr/bin/env python

import pickle
from dnn_app_utils_v2 import *
from p2 import *


if __name__ == '__main__':
    train_x_orig, train_y, test_x_orig, test_y, classes = load_data()

    # Explore your dataset 
    m_train = train_x_orig.shape[0]
    num_px = train_x_orig.shape[1]
    m_test = test_x_orig.shape[0]

    print ("Number of training examples: " + str(m_train))
    print ("Number of testing examples: " + str(m_test))
    print ("Each image is of size: (" + str(num_px) + ", " + str(num_px) + ", 3)")
    print ("train_x_orig shape: " + str(train_x_orig.shape))
    print ("train_y shape: " + str(train_y.shape))
    print ("test_x_orig shape: " + str(test_x_orig.shape))
    print ("test_y shape: " + str(test_y.shape))

    # Reshape the training and test examples 
    train_x_flatten = train_x_orig.reshape(train_x_orig.shape[0], -1).T   # The "-1" makes reshape flatten the remaining dimensions
    test_x_flatten = test_x_orig.reshape(test_x_orig.shape[0], -1).T

    # Standardize data to have feature values between 0 and 1.
    train_x = train_x_flatten/255.
    test_x = test_x_flatten/255.

    print ("train_x's shape: " + str(train_x.shape))
    print ("test_x's shape: " + str(test_x.shape))

    # 2 layer model
    if 0:
        ### CONSTANTS DEFINING THE MODEL ####
        n_x = 12288     # num_px * num_px * 3
        n_h = 7
        n_y = 1
        layers_dims = (n_x, n_h, n_y)

        parameters = two_layer_model(train_x, train_y, layers_dims = (n_x, n_h, n_y), num_iterations = 2500, print_cost=True)
        predictions_train = predict(train_x, train_y, parameters)
        predictions_test = predict(test_x, test_y, parameters)
        
        pickle.dump(parameters, '2_layer_model.pkl')

    # L layer model
    if 1:
        ### CONSTANTS ###
        layers_dims = [12288, 20, 7, 5, 1] #  5-layer model

        parameters = L_layer_model(train_x, train_y, layers_dims, num_iterations = 2500, print_cost = True)
        #parameters = L_layer_model(train_x, train_y, layers_dims, num_iterations=2500, batch_size=100, print_cost = True)
        pred_train = predict(train_x, train_y, parameters)
        pred_test = predict(test_x, test_y, parameters)

        # print_mislabeled_images(classes, test_x, test_y, pred_test)
        with open('l_layer_model.pkt', 'wb') as f:
            pickle.dump(parameters, f)

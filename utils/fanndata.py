#!/usr/bin/env python

from dnn_app_utils_v2 import load_data


def convert_to_fann_data(X, Y, filename):
    with open(filename, 'w') as f:
        n = X.shape[0]  # number of features
        m = X.shape[1] # number of examples
        k = Y.shape[0] # number of classifiers
        f.write('%u %u %u\n' % (m, n, k))

        for i in range(m):
            for j in range(n):
                f.write('%f ' % X[j,i])
            f.write('\n')
            for j in range(k):
                f.write('%f ' % Y[j, i])
            f.write('\n')


# Load the data
train_x_orig, train_y, test_x_orig, test_y, classes = load_data()

# Reshape the training and test examples 
train_x_flatten = train_x_orig.reshape(train_x_orig.shape[0], -1).T   # The "-1" makes reshape flatten the remaining dimensions
test_x_flatten = test_x_orig.reshape(test_x_orig.shape[0], -1).T

# Standardize data to have feature values between 0 and 1.
train_x = train_x_flatten/255.
test_x = test_x_flatten/255.

convert_to_fann_data(train_x, train_y, '../datasets/cats.train')
convert_to_fann_data(test_x, test_y, '../datasets/cats.test')


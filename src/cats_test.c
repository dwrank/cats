/*
Fast Artificial Neural Network Library (fann)
Copyright (C) 2003-2016 Steffen Nissen (steffen.fann@gmail.com)

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <stdio.h>

//#include "fann.h"
#include "floatfann.h"

int main()
{
	struct fann *ann;
	struct fann_train_data *data;
	fann_type *prediction;

	unsigned int i = 0;
    unsigned int m = 0;
    float accuracy = 0;
    float y = 0;

	printf("Creating network.\n");
	ann = fann_create_from_file("cats.net");

	printf("Testing network.\n");

	data = fann_read_train_from_file("../datasets/cats.test");
    m = fann_length_train_data(data);

	fann_reset_MSE(ann);
	for(i = 0; i < m; i++)
	{
		fann_test(ann, data->input[i], data->output[i]);
	}
	printf("MSE error on test data: %f\n", fann_get_MSE(ann));

	for(i = 0; i < m; i++)
	{
        prediction = fann_run(ann, data->input[i]);
        y = *data->output[i];
        printf("%f %f", *prediction, y);
        if ((*prediction >= 0.5 && y == 1) || (*prediction < 0.5 && y == 0))
        {
            printf("\n");
            accuracy++;
        }
        else { printf(" <-- ERROR\n"); }
    }

    accuracy /= m;
    printf("accuracy: %f\n", accuracy);

	printf("Cleaning up.\n");
	fann_destroy_train(data);
	fann_destroy(ann);

	return 0;
}

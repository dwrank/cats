/*
Fast Artificial Neural Network Library (fann)
Copyright (C) 2003-2016 Steffen Nissen (steffen.fann@gmail.com)

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <stdio.h>

#include "fann.h"

int main()
{
	const unsigned int num_layers = 5;
    unsigned int layers[] = {12288, 20, 7, 5, 1};
	const float desired_error = (const float) 0.00;
	struct fann *ann;
	struct fann_train_data *train_data;

	printf("Creating network.\n");

	train_data = fann_read_train_from_file("../datasets/cats.train");

	ann = fann_create_standard_array(num_layers, layers);

	printf("Training network.\n");

    fann_set_activation_function_hidden(ann, FANN_SIGMOID_SYMMETRIC_STEPWISE);
    fann_set_activation_function_output(ann, FANN_SIGMOID);

	fann_train_on_data(ann, train_data, 2500, 50, desired_error);

	printf("Saving network.\n");

	fann_save(ann, "cats.net");

	printf("Cleaning up.\n");
	fann_destroy_train(train_data);
	fann_destroy(ann);

	return 0;
}
